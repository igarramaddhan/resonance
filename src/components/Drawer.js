// @flow
import * as React from 'react';
import styled from 'styled-components';
import Close from 'react-feather/dist/icons/x';

const DrawerContainer = styled.div`
  top: 0;
  left: ${props => (props.active ? 0 : '-300px')};
  z-index: 1;
  width: 300px;
  height: 100vh;
  position: fixed;
  background-color: white;
  transition: 250ms ease-in;

  h3 {
    margin: 0;
  }
`;

const TopContent = styled.div`
  height: 56px;
  display: flex;
  padding: 0 16px;
  align-items: center;
  flex-direction: row;
  border-bottom: 1px solid #eee;
  justify-content: space-between;
`;

const IconContainer = styled.div`
  cursor: pointer;
`;

const LoginContainer = styled.div``;

const Drawer = (props: {active: boolean, toggleDrawer: () => void}) => (
  <DrawerContainer active={props.active}>
    <TopContent>
      <h3>Menu</h3>
      <IconContainer>
        <Close onClick={props.toggleDrawer} />
      </IconContainer>
    </TopContent>
    <LoginContainer />
  </DrawerContainer>
);

export default Drawer;
