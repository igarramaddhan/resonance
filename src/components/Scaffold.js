// @flow
import * as React from 'react';
import styled from 'styled-components';
import Header from './Header';
import Drawer from './Drawer';

const ScaffoldContainer = styled.div`
  padding-top: 56px;
`;

type Props = {
  children: any,
};

const Scaffold = (props: Props) => {
  const [isDrawerActive, setIsDrawerActive] = React.useState(false);
  return (
    <ScaffoldContainer>
      <Drawer
        active={isDrawerActive}
        toggleDrawer={() => setIsDrawerActive(!isDrawerActive)}
      />
      <Header toggleDrawer={() => setIsDrawerActive(!isDrawerActive)} />
      {props.children}
    </ScaffoldContainer>
  );
};
export default Scaffold;
