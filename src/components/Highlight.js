// @flow
import * as React from 'react';
import styled from 'styled-components';

const HighlightContainer = styled.div`
  height: 44px;
  display: flex;
  background-color: #fafafa;
`;

const HighlightList = styled.ul`
  margin: 0;
  padding: 0 16px;
  width: 100%;
  display: flex;
  flex-wrap: nowrap;
  overflow-x: auto;
  scrollbar-width: none;
`;

const HighlightItemContainer = styled.li`
  margin: 0;
  height: 44px;
  display: flex;
  flex: 0 0 auto;
  align-items: center;
  justify-content: center;
  padding-right: 8px;
`;

const HighlightItemBullet = styled.div`
  width: 6px;
  height: 6px;
  border-radius: 3px;
  margin: 0 4px 0 0;
  display: inline-block;
  background-color: #d14e4e;
`;

type Props = {
  items: {text: string}[],
};

const HighlightItem = (props: {text: string}) => (
  <HighlightItemContainer>
    <HighlightItemBullet />
    {props.text}
  </HighlightItemContainer>
);

const Highlight = (props: Props) => (
  <HighlightContainer>
    <HighlightList>
      {props.items.map((val, idx) => (
        <HighlightItem key={idx} text={val.text} />
      ))}
    </HighlightList>
  </HighlightContainer>
);

export default Highlight;
