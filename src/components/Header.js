// @flow
import * as React from 'react';
import styled from 'styled-components';
import Menu from 'react-feather/dist/icons/menu';
import Search from 'react-feather/dist/icons/search';

const HeaderContainer = styled.div`
  top: 0;
  left: 0;
  right: 0;
  height: 56px;
  display: flex;
  position: fixed;
  flex-direction: row;
  background-color: white;
  border-bottom: 1px solid #eee;
`;

const TilteContainer = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const IconContainer = styled.a`
  width: 56px;
  height: 56px;
  display: flex;
  cursor: pointer;
  align-items: center;
  justify-content: center;
`;

const Header = (props: {toggleDrawer: () => void}) => (
  <HeaderContainer>
    <IconContainer onClick={props.toggleDrawer}>
      <Menu />
    </IconContainer>
    <TilteContainer>
      <img src={require('../assets/logo.png')} />
    </TilteContainer>
    <IconContainer>
      <Search />
    </IconContainer>
  </HeaderContainer>
);

export default Header;
