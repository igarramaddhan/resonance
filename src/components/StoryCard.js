// @flow
import * as React from 'react';
import styled from 'styled-components';
import Heart from 'react-feather/dist/icons/heart';
import Comment from 'react-feather/dist/icons/message-circle';
import Share from 'react-feather/dist/icons/share-2';

const StoryCardContainer = styled.div`
  display: flex;
  padding: 16px 0;
  margin-top: 4px;
  flex-direction: row;
  border-top: 1px solid #eee;
`;

const Image = styled.img`
  width: 103px;
  height: 103px;
  display: flex;
  object-fit: cover;
  margin-right: 16px;
  margin-right: 16px;
`;

const StoryTitle = styled.h3`
  margin: 0;
  font-size: 19px;
`;

const AuthorContainer = styled.div`
  display: flex;
  margin: 8px 0;
  align-items: center;
`;

const IconsContainer = styled.div`
  display: flex;
  align-items: center;

  & > * {
    margin-right: 8px;
  }

  span {
    margin-left: 8px;
  }
`;

const StoryCard = () => (
  <StoryCardContainer>
    <div style={{flex: 1}}>
      <StoryTitle>Tiang Beton Tol Bogor Outer Ring Road Ambruk</StoryTitle>

      <AuthorContainer>
        <div
          style={{
            width: 25,
            height: 25,
            backgroundColor: 'grey',
            borderRadius: 12.5,
            marginRight: 8,
          }}
        />
        <span>kumparanBISNIS</span>
      </AuthorContainer>
      <IconsContainer>
        <Heart size={25} />
        <Comment size={25} />
        <Share size={25} />
        <span>sehari</span>
      </IconsContainer>
    </div>
    <Image src="https://blue.kumparan.com/image/upload/fl_progressive,fl_lossy,c_fill,q_auto:best,w_640/v1562724854/b5sz5kjkqnxg1malzmip.jpg" />
  </StoryCardContainer>
);

export default StoryCard;
