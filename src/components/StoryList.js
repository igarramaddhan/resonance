import * as React from 'react';
import styled from 'styled-components';
import StoryCard from './StoryCard';

const StoryListContainer = styled.div`
  padding: 16px;
`;

const StoryListTitle = styled.h2``;

const List = styled.ul`
  margin: 0;
  padding: 0;
`;

const StoryList = () => (
  <StoryListContainer>
    <StoryListTitle>Trending</StoryListTitle>
    <List>
      {[0, 0, 0, 0, 0].map((val, idx) => (
        <StoryCard key={idx} />
      ))}
    </List>
  </StoryListContainer>
);

export default StoryList;
