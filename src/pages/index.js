// @flow
import React from 'react';
import '../style/index.scss';
import Scaffold from '../components/Scaffold';
import Highlight from '../components/Highlight';
import StoryList from '../components/StoryList';

export default function Index() {
  return (
    <Scaffold>
      <Highlight
        items={[
          {text: 'Serbuan Sampah Impor'},
          {text: 'Bambang Mencari Ibunya'},
          {text: 'Bambang Beli Ikan Cupang'},
        ]}
      />
      <div className="content-container">
        <h2 className="welcome">Welcome to Your React App Starter</h2>
        <p className="instruction">You can modify anything here as you wish</p>
      </div>
      <StoryList />
    </Scaffold>
  );
}
